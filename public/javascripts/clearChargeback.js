function limpar() {

    var ec = document.getElementsByClassName('chb-ec');
    var ambiente = document.getElementsByClassName('chb-ambiente');
    var tipo = document.getElementsByClassName('chb-tipo');;
    var codigo = document.getElementsByClassName('chb-cod-ajuste');
    var motivo = document.getElementsByClassName('chb-motivo');
    
    //verificar se o checkbox manter está checado
    //se estiver, não limpa o campo 'EC', caso contrário, limpa.          
    if(!ec[0].checked) {
        document.getElementById('ec').value = '';
    }

    //verificar se o checkbox manter está checado
    //se estiver, não limpa o campo 'AMBIENTE', caso contrário, limpa.          
    if(!ambiente[0].checked) {
        document.getElementById('ambiente').selectedIndex = '0'
    }

    //verificar se o checkbox manter está checado
    //se estiver, não limpa o campo 'TIPO DE PAGAMENTO', caso contrário, limpa.          
    if(!tipo[0].checked) {
        document.getElementById('tipo').selectedIndex = '0'
    }

    //verificar se o checkbox manter está checado
    //se estiver, não limpa o campo 'QUANTIDADE DE TRANSAÇÕES', caso contrário, limpa.          
    if(!codigo[0].checked) {
        document.getElementById('cod-ajuste').selectedIndex = '0'
    }

    if(!motivo[0].checked) {
        document.getElementById('motivo').selectedIndex = '0'
    }
    
    document.getElementById('codigo').value = ''

}