var express = require('express');
var router = express.Router();

/* GET login page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET home page. */
router.get('/home', function(req, res, next) {
  res.render('home', { title: 'Express' });
});

/* GET venda page. */
router.get('/venda', function(req, res, next) {
  res.render('venda', { title: 'Express' });
});

/* GET rejeicao page. */
router.get('/rejeicao', function(req, res, next) {
  res.render('rejeicao', { title: 'Express' });
});

/* GET chargeback page. */
router.get('/chargeback', function(req, res, next) {
  res.render('chargeback', { title: 'Express' });
});

/* GET produtos page. */
router.get('/product', function(req, res, next) {
  res.render('product', { title: 'Express' });
});

module.exports = router;
