
//injetar as informações na api
function injetar() {

    var http = new XMLHttpRequest();

    //endpoint
    var url = 'https://5c2cd1f2b8051f0014cd46b3.mockapi.io/sale/';

    http.open('POST', url, true);

    //Send the proper header information along with the request
    http.setRequestHeader('Content-type', 'application/json');

    http.onreadystatechange = function () { //Call a function when the state changes.
        if (http.readyState == 4 && http.status == 200) {

        }

    }

    //pegando valores obrigatorios a serem enviados
    var json = new Object();

    var ec = document.getElementById('ec').value;
    var bandeira = document.getElementById("bandeira").options[document.getElementById("bandeira").selectedIndex].text
    var valor = document.getElementById('valor').value;
    var tipo = document.getElementById("tipo").options[document.getElementById("tipo").selectedIndex].text
    var parcelas = document.getElementById('parcelas').value;
    var quantidade = document.getElementById('quantidade').value;
    var ambiente = document.getElementById("ambiente").options[document.getElementById("ambiente").selectedIndex].text

    //validando se os campos obrigatorios estão preenchidos corretamente
    //se todos estiverem, envia, caso contrário, mostra uma mensagem na tela.
    if(ec == "" || bandeira == "Selecione uma opção..." || valor == "" 
    || tipo == "Selecione uma bandeira..." || parcelas == "" || quantidade == "" || ambiente == "Selecione uma opção...") {
        document.getElementById("alerta").innerHTML = "<label id=\"mensagem\">Existem campos obrigatórios (*) não preenchidos.</label>"
    } else {

    document.getElementById("mensagem").innerHTML = "<span></span>";

    json.ec = ec;
    json.valor = valor;
    json.parcelas = parcelas;
    json.terminal = document.getElementById('terminal').value;
    json.card = document.getElementById('cartao').value;
    json.quantidade = quantidade;
    
    json.bandeira = bandeira;
    json.tipopagamento = tipo;
    json.loja = document.getElementById("loja").options[document.getElementById("loja").selectedIndex].text
    json.ambiente = ambiente;

    var params = JSON.stringify(json);

    http.send(params)

    var elemento = document.getElementById("alerta");
    elemento.classList.remove("alert-danger");
    elemento.classList.add("alert-success");
    elemento.innerHTML = "<label id=\"mensagem\">As informações foram enviadas com sucesso.</label>"

    document.getElementById("parcelas").disabled = false;

    limpar();

    }
}

//procurar tipo de pagamento conforme a bandeira selecionada
function tipos(bandeira) {

    suggestion(bandeira);

    var http = new XMLHttpRequest();
    var url = 'https://5c2cd1f2b8051f0014cd46b3.mockapi.io/' + bandeira.toLowerCase() + '/';

    http.open('GET', url, true);

    //Send the proper header information along with the request
    http.setRequestHeader('Content-type', 'application/json');
    http.responseType = "text";

    http.send("");

    http.onreadystatechange = function () { //Call a function when the state changes.
        var arr = JSON.parse(http.response);

        var images = "<option>Escolha uma opção...</option>";
        var i;

        //verificar se no retorno há nenhum registro
        if (arr != "Not found") {

            for (i = 0; i < arr.length; i++) {
                var x = i + 1;
                images += "<option>" + arr[i].name + "</option>";
            }

            images += "</select>";
        }

        images += "</select>";
        document.getElementById("tipo").innerHTML = images;
        document.getElementById("tipo").disabled = false;

    }
}