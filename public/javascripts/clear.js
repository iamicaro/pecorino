function limpar() {

    var ec = document.getElementsByClassName('chb-ec');
    var ambiente = document.getElementsByClassName('chb-ambiente');
    var bandeira = document.getElementsByClassName('chb-bandeira');
    var tipo = document.getElementsByClassName('chb-tipo');
    var valor = document.getElementsByClassName('chb-valor');
    var quantidade = document.getElementsByClassName('chb-quantidade');
    
    //verificar se o checkbox manter está checado
    //se estiver, não limpa o campo 'EC', caso contrário, limpa.          
    if(!ec[0].checked) {
        document.getElementById('ec').value = '';
    }

    //verificar se o checkbox manter está checado
    //se estiver, não limpa o campo 'AMBIENTE', caso contrário, limpa.          
    if(!ambiente[0].checked) {
        document.getElementById('ambiente').selectedIndex = '0'
    }

    //verificar se o checkbox manter está checado
    //se estiver, não limpa o campo 'BANDEIRA', caso contrário, limpa.          
    if(!bandeira[0].checked) {
        document.getElementById('bandeira').selectedIndex = '0'
    }

    //verificar se o checkbox manter está checado
    //se estiver, não limpa o campo 'TIPO DE PAGAMENTO', caso contrário, limpa.          
    if(!tipo[0].checked) {
        document.getElementById('tipo').selectedIndex = '0'
        document.getElementById('parcelas').value = ''
    }

    //verificar se o checkbox manter está checado
    //se estiver, não limpa o campo 'VALOR DA TRANSAÇÃO', caso contrário, limpa.          
    if(!valor[0].checked) {
        document.getElementById('valor').value = ''
    }

    //verificar se o checkbox manter está checado
    //se estiver, não limpa o campo 'QUANTIDADE DE TRANSAÇÕES', caso contrário, limpa.          
    if(!quantidade[0].checked) {
        document.getElementById('quantidade').value = ''
    }

    document.getElementById('terminal').value = ''
    document.getElementById('cartao').value = ''
    document.getElementById('loja').selectedIndex = '0'

}